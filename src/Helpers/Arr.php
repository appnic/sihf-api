<?php

namespace appnic\SihfApi\Helpers;

class Arr
{
    /**
     * Flatten a multi-dimensional associative array with dots.
     *
     * @param  array $array
     * @param  string $prepend
     * @return array
     */
    public static function dot($array, $prepend = '')
    {
        $results = [];

        foreach ($array as $key => $value) {
            if (is_array($value) && !empty($value)) {
                $results = array_merge($results, static::dot($value, $prepend . $key . '.'));
            } else {
                $results[$prepend . $key] = $value;
            }
        }

        return $results;
    }

    /**
     * Searches the haystack for the dotted-style needle
     *
     * @param string $needle    The needle to search for (e.g. "first.second.third")
     * @param array $haystack   The array to search in
     * @return mixed|null       If anything was found, return it. Otherwise null.
     */
    public static function dotSearch(string $needle, array $haystack)
    {
        $splittedNeedle = explode('.', $needle);
        $currentHaystack = $haystack;

        for ($i = 0; $i < count($splittedNeedle)-1; $i++) {
            if(array_key_exists($splittedNeedle[$i], $currentHaystack)) {
                $currentHaystack = $currentHaystack[$splittedNeedle[$i]];

                if(!is_array($currentHaystack)) {
                    return null;
                }
            }
        }

        if(array_key_exists(end($splittedNeedle), $currentHaystack)) {
            return $currentHaystack[end($splittedNeedle)];
        }
    }
}