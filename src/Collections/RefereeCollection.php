<?php

namespace appnic\SihfApi\Collections;

use appnic\SihfApi\Resources\Referee;

class RefereeCollection extends Collection
{
    public $acceptsType = "appnic\SihfApi\Resources\Referee";

    public function __construct(Referee ...$referees)
    {
        parent::__construct($referees);
    }
}