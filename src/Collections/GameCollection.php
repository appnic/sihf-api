<?php

namespace appnic\SihfApi\Collections;

use appnic\SihfApi\Resources\Game;

class GameCollection extends Collection
{
    public $acceptsType = "appnic\SihfApi\Resources\Game";

    public function __construct(Game ...$games)
    {
        parent::__construct($games);
    }
}