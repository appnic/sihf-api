<?php

namespace appnic\SihfApi\Resources;

use appnic\SihfApi\Collections\GoalCollection;
use appnic\SihfApi\Collections\PenaltyCollection;
use appnic\SihfApi\Collections\PlayerCollection;
use appnic\SihfApi\Collections\RefereeCollection;
use Carbon\Carbon;

class Game extends Resource
{
    const STATUS_BEFORE_START = 0;
    const STATUS_FIRST_PERIOD = 1;
    const STATUS_FIRST_INTERMISSION = 2;
    const STATUS_SECOND_PERIOD = 3;
    const STATUS_SECOND_INTERMISSION = 4;
    const STATUS_THIRD_PERIOD = 5;
    const STATUS_THIRD_INTERMISSION = 6;
    const STATUS_OVERTIME = 7;
    const STATUS_SHOOTOUT = 8;
    const STATUS_END = 9;
    const STATUS_UNKNOWN = 99;

    /**
     * @var int $id
     */
    private $id;

    /**
     * @var int
     */
    private $status = self::STATUS_UNKNOWN;

    /**
     * @var bool
     */
    private $cancelled;

    /**
     * @var Team $homeTeam
     */
    private $homeTeam;

    /**
     * @var Team $awayTeam
     */
    private $awayTeam;

    /**
     * @var array $result
     */
    private $result = [[0,0,0],[0,0,0]];

    /**
     * @var PlayerCollection
     */
    private $homeTeamPlayers;

    /**
     * @var PlayerCollection
     */
    private $awayTeamPlayers;

    /**
     * @var RefereeCollection
     */
    private $referees;

    /**
     * @var Venue
     */
    private $venue;

    /**
     * @var Carbon
     */
    private $startDateTime;

    /**
     * @var Carbon
     */
    private $endDateTime;

    /**
     * @var GoalCollection
     */
    private $goals;

    /**
     * @var PenaltyCollection
     */
    private $penalties;

    /**
     * @var boolean
     */
    private $overtime;

    /**
     * @var boolean
     */
    private $shootout;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function isCancelled(): bool
    {
        return $this->cancelled;
    }

    /**
     * @param bool $cancelled
     */
    public function setCancelled(bool $cancelled): void
    {
        $this->cancelled = $cancelled;
    }

    /**
     * @return Team
     */
    public function getHomeTeam(): Team
    {
        return $this->homeTeam;
    }

    /**
     * @param Team $homeTeam
     */
    public function setHomeTeam(Team $homeTeam): void
    {
        $this->homeTeam = $homeTeam;
    }

    /**
     * @return Team
     */
    public function getAwayTeam(): Team
    {
        return $this->awayTeam;
    }

    /**
     * @param Team $awayTeam
     */
    public function setAwayTeam(Team $awayTeam): void
    {
        $this->awayTeam = $awayTeam;
    }

    /**
     * @return array
     */
    public function getResult(): array
    {
        return $this->result;
    }

    /**
     * @param array $result
     */
    public function setResult(array $result): void
    {
        $this->result = $result;
    }

    /**
     * @return PlayerCollection
     */
    public function getHomeTeamPlayers(): PlayerCollection
    {
        return $this->homeTeamPlayers;
    }

    /**
     * @param PlayerCollection $homeTeamPlayers
     */
    public function setHomeTeamPlayers(PlayerCollection $homeTeamPlayers): void
    {
        $this->homeTeamPlayers = $homeTeamPlayers;
    }

    /**
     * @return PlayerCollection
     */
    public function getAwayTeamPlayers(): PlayerCollection
    {
        return $this->awayTeamPlayers;
    }

    /**
     * @param PlayerCollection $awayTeamPlayers
     */
    public function setAwayTeamPlayers(PlayerCollection $awayTeamPlayers): void
    {
        $this->awayTeamPlayers = $awayTeamPlayers;
    }

    /**
     * @return RefereeCollection
     */
    public function getReferees(): RefereeCollection
    {
        return $this->referees;
    }

    /**
     * @param RefereeCollection $referees
     */
    public function setReferees(RefereeCollection $referees): void
    {
        $this->referees = $referees;
    }

    /**
     * @return Venue
     */
    public function getVenue(): Venue
    {
        return $this->venue;
    }

    /**
     * @param Venue $venue
     */
    public function setVenue(Venue $venue): void
    {
        $this->venue = $venue;
    }

    /**
     * @return Carbon
     */
    public function getStartDateTime(): Carbon
    {
        return $this->startDateTime;
    }

    /**
     * @param Carbon $startDateTime
     */
    public function setStartDateTime(Carbon $startDateTime): void
    {
        $this->startDateTime = $startDateTime;
    }

    /**
     * @return Carbon
     */
    public function getEndDateTime(): Carbon
    {
        return $this->endDateTime;
    }

    /**
     * @param Carbon $endDateTime
     */
    public function setEndDateTime(Carbon $endDateTime): void
    {
        $this->endDateTime = $endDateTime;
    }

    /**
     * @return GoalCollection
     */
    public function getGoals(): GoalCollection
    {
        return $this->goals;
    }

    /**
     * @param GoalCollection $goals
     */
    public function setGoals(GoalCollection $goals): void
    {
        $this->goals = $goals;
    }

    /**
     * @return PenaltyCollection
     */
    public function getPenalties(): PenaltyCollection
    {
        return $this->penalties;
    }

    /**
     * @param PenaltyCollection $penalties
     */
    public function setPenalties(PenaltyCollection $penalties): void
    {
        $this->penalties = $penalties;
    }

    /**
     * Returns the total numbers of goals of the home team (calculated from getResult() array)
     * @return int
     */
    public function getHomeGoalTotal() {
        $goals = 0;
        foreach ($this->getResult()[0] as $count) {
            $goals += (int)$count;
        }

        return $goals;
    }

    /**
     * Returns the total numbers of goals of the away team (calculated from getResult() array)
     * @return int
     */
    public function getAwayGoalTotal() {
        $goals = 0;
        foreach ($this->getResult()[1] as $count) {
            $goals += (int)$count;
        }

        return $goals;
    }

    /**
     * @return bool
     */
    public function isOvertime(): bool
    {
        return $this->overtime;
    }

    /**
     * @param bool $overtime
     */
    public function setOvertime(bool $overtime): void
    {
        $this->overtime = $overtime;
    }

    /**
     * @return bool
     */
    public function isShootout(): bool
    {
        return $this->shootout;
    }

    /**
     * @param bool $shootout
     */
    public function setShootout(bool $shootout): void
    {
        $this->shootout = $shootout;
    }
}