<?php

namespace appnic\SihfApi\Resources;

class Goal extends Resource
{
    /**
     * @var int
     */
    private $time;

    /**
     * @var int
     */
    private $scorerId;

    /**
     * @var int
     */
    private $firstAssistId;

    /**
     * @var int
     */
    private $secondAssistId;

    /**
     * @return int
     */
    public function getTime(): int
    {
        return $this->time;
    }

    /**
     * @param int $time
     */
    public function setTime(int $time): void
    {
        $this->time = $time;
    }

    /**
     * @return int
     */
    public function getScorerId(): int
    {
        return $this->scorerId;
    }

    /**
     * @param int $scorerId
     */
    public function setScorerId(int $scorerId): void
    {
        $this->scorerId = $scorerId;
    }

    /**
     * @return int
     */
    public function getFirstAssistId(): int
    {
        return $this->firstAssistId;
    }

    /**
     * @param int $firstAssistId
     */
    public function setFirstAssistId(int $firstAssistId): void
    {
        $this->firstAssistId = $firstAssistId;
    }

    /**
     * @return int
     */
    public function getSecondAssistId(): int
    {
        return $this->secondAssistId;
    }

    /**
     * @param int $secondAssistId
     */
    public function setSecondAssistId(int $secondAssistId): void
    {
        $this->secondAssistId = $secondAssistId;
    }
}