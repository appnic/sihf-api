<?php

namespace appnic\SihfApi\Mappers;

use appnic\SihfApi\Resources\Venue;

class VenueMapper extends Mapper
{
    function initializeMappings(): void
    {
        $this->addDirectMapping('name');
        $this->addDirectMapping('spectators');
        $this->addDirectMapping('street');
        $this->addDirectMapping('zip');
        $this->addDirectMapping('city');
        $this->addDirectMapping('country');
    }

    /**
     * @param array $source
     * @return Venue
     */
    function map(array $source): \appnic\SihfApi\Resources\Resource
    {
        $venue = new Venue();
        return $this->performMapping($venue, $source);
    }
}