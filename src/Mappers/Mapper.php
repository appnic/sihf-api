<?php

namespace appnic\SihfApi\Mappers;

use appnic\SihfApi\Helpers\Arr;
use appnic\SihfApi\Resources\Resource;

abstract class Mapper
{
    protected $mappings = array();

    public function __construct()
    {
        $this->initializeMappings();
    }

    protected function addMapping(string $index, \Closure $closure) {
        $this->mappings[$index] = $closure;
    }

    protected function addDirectMapping(string $index, string $target = null) {
        if($target == null) {
            $target = $index;
        }

        $this->mappings[$index] = $target;
    }

    protected function addMapperMapping(string $index, string $target, Mapper $mapper) {
        $this->addMapping($index, function($resource, $value) use ($mapper, $target) {
            // Set the value by calling the setter
            $methodName = 'set'.ucfirst($target);
            if(method_exists($resource, $methodName)) {
                $resource->$methodName($mapper->map($value));
            }
        });
    }

    protected function performMapping(Resource $resource, array $source) : \appnic\SihfApi\Resources\Resource {
        $flattenedSource = Arr::dot($source);

        foreach($this->mappings as $field=>$mapping) {
            $value = null;
            if(array_key_exists($field, $flattenedSource)) {
                // If the flattened source contains the field name, then return it...
                $value = $flattenedSource[$field];
            } elseif(array_key_exists($field, $source)) {
                // ...otherwise check if the initial source array countains the field
                $value = $source[$field];
            } else {
                // If none of the above is true, the field may be a dot-style field, but
                // not actually a field but a whole array. Therefore manually search for the field.
                $search = Arr::dotSearch($field, $source);
                if($search != null) {
                    $value = $search;
                }
            }

            if(!is_null($value)) {
                if($mapping instanceof \Closure) {
                    // If the mapping is a closure, call the closure
                    $mapping($resource, $value);
                } else {
                    // If the mapping is a field name, set the value by calling the setter
                    $methodName = 'set'.ucfirst($mapping);

                    // Check if the setter method actually exists
                    if(method_exists($resource, $methodName)) {
                        // Use reflection to check if the setter method has a parameter and to determine its type
                        $refClass = new \ReflectionClass($resource);
                        $methodParams = $refClass->getMethod($methodName)->getParameters();
                        if(count($methodParams) > 0) {
                            settype($value ,$methodParams[0]->getType()->getName());
                            $resource->$methodName($value);
                        }
                    }
                }
            }
        }

        return $resource;
    }

    abstract function initializeMappings() : void;
    abstract function map(array $source) : \appnic\SihfApi\Resources\Resource;
}