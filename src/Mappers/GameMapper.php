<?php

namespace appnic\SihfApi\Mappers;

use appnic\SihfApi\Collections\GoalCollection;
use appnic\SihfApi\Collections\PenaltyCollection;
use appnic\SihfApi\Collections\PlayerCollection;
use appnic\SihfApi\Collections\RefereeCollection;
use appnic\SihfApi\Resources\Game;
use Carbon\Carbon;
use function foo\func;

class GameMapper extends Mapper
{
    function initializeMappings(): void
    {
        $this->addDirectMapping('gameId','id');
        $this->addDirectMapping('status.canceled', 'cancelled');
        $this->addMapperMapping('details.venue', 'venue', new VenueMapper());
        $this->addMapperMapping('details.homeTeam', 'homeTeam', new TeamMapper());
        $this->addMapperMapping('details.awayTeam', 'awayTeam', new TeamMapper());

        $this->addMapping('status', function(Game $game, array $status) {
            $startDate = new Carbon($status['startDateTime'], 'Europe/Zurich');
            $endDate = new Carbon($status['endDateTime'], 'Europe/Zurich');
            $game->setStartDateTime($startDate);
            $game->setEndDateTime($endDate);

            $statusPercentages = [
                0 => Game::STATUS_BEFORE_START,
                17 => Game::STATUS_FIRST_PERIOD,
                33 => Game::STATUS_FIRST_INTERMISSION,
                50 => Game::STATUS_SECOND_PERIOD,
                67 => Game::STATUS_SECOND_INTERMISSION,
                83 => Game::STATUS_THIRD_PERIOD,
                88 => Game::STATUS_OVERTIME,
                100 => Game::STATUS_END,
            ];
            $game->setStatus($statusPercentages[$status['percent']]);
        });

        $this->addMapping('players', function(Game $game, array $rawPlayers) {
            $homeTeamPlayers = new PlayerCollection();
            $awayTeamPlayers = new PlayerCollection();

            $playerMapper = new PlayerMapper();
            $homeTeam = $game->getHomeTeam();
            $awayTeam = $game->getAwayTeam();
            foreach($rawPlayers as $rawPlayer) {
                $player = $playerMapper->map($rawPlayer);
                if($homeTeam->getId() == $rawPlayer['teamId']) {
                    $player->setTeam($homeTeam);
                    $homeTeamPlayers[] = $player;
                } else {
                    $player->setTeam($awayTeam);
                    $awayTeamPlayers[] = $player;
                }
            }

            $game->setHomeTeamPlayers($homeTeamPlayers);
            $game->setAwayTeamPlayers($awayTeamPlayers);
        });

        $this->addMapping('summary.periods', function(Game $game, array $periods) {
            $goalCollection = new GoalCollection();
            $penaltyCollection = new PenaltyCollection();

            $goalMapper = new GoalMapper();
            $penaltyMapper = new PenaltyMapper();
            foreach ($periods as $period) {
                foreach($period['goals'] as $rawGoal) {
                    $goalCollection[] = $goalMapper->map($rawGoal);
                }

                foreach($period['fouls'] as $rawPenalty) {
                    $penaltyCollection[] = $penaltyMapper->map($rawPenalty);
                }
            }

            $game->setGoals($goalCollection);
            $game->setPenalties($penaltyCollection);
            $game->setOvertime(count($periods)>3);
        });

        $this->addMapping('summary.shootout', function(Game $game, array $shootout) {
            $game->setShootout(count($shootout['shoots']) > 0);
        });

        $this->addMapping('details', function(Game $game, array $details) {
            $collection = new RefereeCollection();
            $mapper = new RefereeMapper();

            foreach ($details['referees'] as $rawReferee) {
                $ref = $mapper->map($rawReferee);
                $ref->setIsLinesman(false);
                $collection[] = $ref;
            }

            foreach ($details['linesmen'] as $rawReferee) {
                $ref = $mapper->map($rawReferee);
                $ref->setIsLinesman(true);
                $collection[] = $ref;
            }

            $game->setReferees($collection);
        });

        $this->addMapping('result.scores', function(Game $game, array $scores) {
            $result = [
                [0,0,0],
                [0,0,0]
            ];

            foreach($scores as $score) {
                $result[0][] = (int)$score['homeTeam'];
                $result[1][] = (int)$score['awayTeam'];
            }

            $game->setResult($result);
        });
    }

    /**
     * @param array $source
     * @return \appnic\SihfApi\Resources\Game
     */
    function map(array $source): \appnic\SihfApi\Resources\Resource
    {
        $game = new Game();
        return $this->performMapping($game, $source);
    }
}