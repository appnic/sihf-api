<?php

namespace appnic\SihfApi\Mappers;

use appnic\SihfApi\Helpers\GameTime;
use appnic\SihfApi\Resources\Goal;
use appnic\SihfApi\Resources\Team;

class GoalMapper extends Mapper
{
    function initializeMappings(): void
    {
        $this->addDirectMapping('scorerLicenceNr', 'scorerId');
        $this->addDirectMapping('assist1LicenceNr', 'firstAssistId');
        $this->addDirectMapping('assist2LicenceNr', 'secondAssistId');
        $this->addMapping('time', function(Goal $goal, string $time) {
            $goal->setTime(GameTime::toSeconds($time));
        });
    }

    /**
     * @param array $source
     * @return Goal
     */
    function map(array $source): \appnic\SihfApi\Resources\Resource
    {
        $goal = new Goal();
        return $this->performMapping($goal, $source);
    }
}