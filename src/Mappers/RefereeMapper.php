<?php

namespace appnic\SihfApi\Mappers;

use appnic\SihfApi\Helpers\GameTime;
use appnic\SihfApi\Resources\Goal;
use appnic\SihfApi\Resources\Penalty;
use appnic\SihfApi\Resources\Referee;
use appnic\SihfApi\Resources\Team;

class RefereeMapper extends Mapper
{
    function initializeMappings(): void
    {
        $this->addDirectMapping('jerseyNumber');
        $this->addDirectMapping('country');

        $this->addMapping('fullName', function(Referee $referee, $value) {
            $nameArray = explode(' ', $value, 2);
            $referee->setLastName($nameArray[1]);
            $referee->setFirstName($nameArray[0]);
        });
    }

    /**
     * @param array $source
     * @return Referee
     */
    function map(array $source): \appnic\SihfApi\Resources\Resource
    {
        $referee = new Referee();
        return $this->performMapping($referee, $source);
    }
}