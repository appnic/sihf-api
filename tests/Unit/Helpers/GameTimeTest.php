<?php

namespace appnic\SihfApi\tests\Unit\Helpers;

use appnic\SihfApi\Helpers\Arr;
use appnic\SihfApi\Helpers\GameTime;
use PHPUnit\Framework\TestCase;

class GameTimeTest extends TestCase
{
    /**
     * @param int $seconds
     * @param string $gametime
     * @dataProvider timeProvider
     * @covers       \appnic\SihfApi\Helpers\GameTime::toSeconds
     */
    public function testToSeconds(int $seconds, string $gametime)
    {
        $this->assertSame($seconds, GameTime::toSeconds($gametime));
    }

    /**
     * @param int $seconds
     * @param string $gametime
     * @dataProvider timeProvider
     * @covers       \appnic\SihfApi\Helpers\GameTime::toGameTime
     */
    public function testToGameTime(int $seconds, string $gametime) {
        $this->assertSame($gametime, GameTime::toGameTime($seconds));
    }

    public function timeProvider()
    {
        return [
            [0, '00:00'],
            [1, '00:01'],
            [3600, '60:00'],
            [3900, '65:00'],
            [632, '10:32'],
            [2000, '33:20']
        ];
    }
}
