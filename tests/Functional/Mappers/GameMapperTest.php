<?php

namespace appnic\SihfApi\tests\Functional;

use appnic\SihfApi\Helpers\Arr;
use appnic\SihfApi\Mappers\GameMapper;
use PHPUnit\Framework\TestCase;

class GameMapperTest extends TestCase
{
    private $json;
    private $flattenedJson;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->json = json_decode(file_get_contents(__DIR__ . '/../Json/gamedetail.json'), JSON_OBJECT_AS_ARRAY);
        $this->flattenedJson = Arr::dot($this->json);
    }

    /**
     * @covers \appnic\SihfApi\Mappers\GameMapper::map
     * @return \appnic\SihfApi\Resources\Game|\appnic\SihfApi\Resources\Resource
     */
    public function testCreate()
    {
        $game = (new GameMapper())->map($this->json);

        $this->assertInstanceOf('\appnic\SihfApi\Resources\Game', $game);

        return $game;
    }
}
