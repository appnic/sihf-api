<?php

namespace appnic\SihfApi\tests\Functional;

use appnic\SihfApi\Helpers\Arr;
use appnic\SihfApi\Mappers\ScheduleMapper;
use appnic\SihfApi\Resources\Schedule;
use PHPUnit\Framework\TestCase;

class ScheduleMapperTest extends TestCase
{
    private $json;
    private $flattenedJson;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->json = json_decode(file_get_contents(__DIR__ . '/../Json/schedule.json'), JSON_OBJECT_AS_ARRAY);
        $this->flattenedJson = Arr::dot($this->json);

    }

    public function testCreate()
    {
        $schedule = (new ScheduleMapper())->map($this->json);

        $this->assertInstanceOf('\appnic\SihfApi\Resources\Schedule', $schedule);

        return $schedule;
    }

    /**
     * @depends testCreate
     * @param Schedule $schedule
     */
    public function testMappings(Schedule $schedule)
    {
        $this->assertSame((int)$this->flattenedJson['filters.0.selected'], $schedule->getSeasonYear());
        $this->assertSame((int)$this->flattenedJson['filters.1.selected'], $schedule->getTeamId());
    }

    /**
     * @depends testCreate
     * @param Schedule $schedule
     */
    public function testGames(Schedule $schedule) {
        $this->assertNotNull($schedule->getGames()[0]);
    }
}
